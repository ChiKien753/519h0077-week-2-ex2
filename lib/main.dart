import 'dart:async';
import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';
import 'package:w2_ex2_519h0077/stream/word_gen_stream.dart';

// Haven't finish yet !
void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({ Key? key }) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  WordGen wordGen = WordGen();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('WordPair generator')),
      body: Text(wordGen.wordPairLength.toString()),
    );
  }

  generateWord() async {
    wordGen.getWords().listen((eventWord) {
      setState(() {
        print(eventWord);
      });
    });
  }
}

