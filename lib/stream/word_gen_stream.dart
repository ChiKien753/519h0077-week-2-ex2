import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';
import 'dart:async';




class WordGen{
  final _randomWordPairs = <WordPair>[];
  final streamController = StreamController<String>();
  int wordPairLength = -1;

  get receiveData => streamController.sink.add;

  WordGen(){
    streamController.stream.listen((value) {
      print(value);
    });
  }

  Stream<WordPair> getWords() async*{
    yield* Stream.periodic(Duration(seconds: 1), (int t){
      print(t);
      _randomWordPairs.addAll(generateWordPairs().take(1));
      wordPairLength = _randomWordPairs.length;
      return _randomWordPairs[wordPairLength];
    });
  }
}
